const express = require('express');
const fs = require('fs');
const router = express.Router();

const path = './messages';

router.get('/', (req, res) => {
  const data = [];
  const messages = [];

  fs.readdir(path, (err, files) => {
    files.forEach(file => {
      data.push(path + '/' + file);
    });

    data.splice(0, data.length - 5);

    for (let key in data) {
      messages.push(JSON.parse(fs.readFileSync(data[key])));
    }

    res.send(messages);
  });
});

router.post('/', (req, res) => {
  const reqBody = req.body;
  reqBody.datetime = new Date();

  const thisDate = new Date().toISOString();
  const fileName = './messages/' + thisDate + '.txt';

  fs.writeFile(fileName, JSON.stringify(reqBody), err => {
    if (err) {
      console.log(err);
    } else {
      console.log('File was saved!');
    }
  });

  res.send(reqBody);
});

module.exports = router;